class Event < ActiveRecord::Base
  after_save :update_product_counters

  belongs_to :product

  enum kind: [:bought, :viewed, :carted]

  # не позволять создавать одинаковые события
  validates :product_id, uniqueness: { scope: [:kind, :group] }


  private
  
    def update_product_counters
      product = Product.find(self.product_id)

      # тип события подставляется сам
      event_count = Event.where(product: product).count

      product.update_attribute("#{self.kind}_count", event_count)
    end
end
