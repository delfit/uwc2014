class ShoppingCart < ActiveRecord::Base
  acts_as_shopping_cart

  def count
  	shopping_cart_items.length
  end
end