class Product < ActiveRecord::Base
  has_many :events


  # отсортировать товары по количеству покупок, просмотров и добавлений в корзину
  scope :ordered, -> {
    ordered_by(:bought).
    ordered_by(:viewed).
    ordered_by(:carted)
  }

  # отсортировать товары по количеству определенных событий
  scope :ordered_by, ->(event_kind) {
    order("`#{event_kind}_count` DESC")
  }


  # товары, что рекомендуются с данным товаром
  def recommended_by(event_kind)
    Product.
    joins(:events).
    where.not(id: self.id).
    where(events: { kind: Event.kinds[event_kind] }).
    where("
      `events`.`group` IN (
        SELECT DISTINCT
          `group`
        FROM `events`
        WHERE `events`.`product_id` = #{self.id} AND
          `events`.`kind` = #{Event.kinds[event_kind]}
      )
    ").
    ordered_by(event_kind)
  end


  # товары, что покупались с данным
  def bought_with
    recommended_by :bought
  end

  # товары, что просматривались с данным
  def viewed_with
    recommended_by :viewed
  end

  # товары, что попадали в корзину с данным
  def carted_with
    recommended_by :carted
  end
end
