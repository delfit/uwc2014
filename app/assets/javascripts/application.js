// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require maskedinput
// 
//= require orders
//= require products
//= require shopping_carts


$(function(){ 
  // $(document).foundation();
  
  // show/hide the cart
  $('[data-action="open-cart"]').click(function(e){
    $('[data-container="cart"]').toggleClass('open');
  });

  // hide the cart if a user clicked outside the cart
  $('body').click(function(e){
    $el = $(e.target);
    if($el.attr('data-action') == 'open-cart') {
      return true;
    }

    if($el.parents('[data-container="cart"]').length < 1) {
      $('[data-container="cart"]').removeClass('open');
    }    
  });

  // set a mask for an field
  $('[data-mask]').each(function(){
    $(this).mask($(this).attr('data-mask'));
  });
});
