class OrdersController < ApplicationController
  def create
    # создать новый заказ
    @order = Order.new order_params
    @order.save

    # создать событие покупки для каждого товара в заказе
    @shopping_cart.shopping_cart_items.each do |item|
      Event.bought.create(
        group: @order.id,
        product_id: item.item_id
      )
    end
    
    # очистить корзину
    @shopping_cart.clear
    
    redirect_to :back, notice: "Ваше замовлення прийнято. Дякуємо за покупку!"
  end

  private
    def order_params
      params.require(:order).permit(:client_name, :client_phone)
    end
end
