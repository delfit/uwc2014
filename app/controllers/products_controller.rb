class ProductsController < ApplicationController
  def index
    @products = Product.ordered
  end

  def show
    @product = Product.find params[:id]
    
    Event.viewed.create(
      group: request.env['rack.session'].id,
      product_id: @product.id
    )
  end
end