class ShoppingCartsController < ApplicationController
  def add
    @product = Product.find params[:id]
    @shopping_cart.add @product, @product.price

    Event.carted.create(
      group: @shopping_cart.id,
      product_id: @product.id
    )

    redirect_to :back
  end

  def clear
    @shopping_cart.clear
    session[:shopping_cart_id] = nil
    redirect_to :back
  end
end
