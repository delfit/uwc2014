README
======

Installation
------------

- установіть [Vagrant](https://www.vagrantup.com/) для вашої системи
- для Linux встановіть останню версію : `vagrant plugin install vagrant-vbguest`
- завантажте та встановіть `uwc2014-delfit.box` :
    1. `vagrant box add uwc2014-delfit uwc2014-delfit.box`
    2. `vagrant init uwc2014-delfit`
    3. - відкрийте `3000` порт, додавши в файл конфігурації `Vagrantfile` :  `config.vm.network :forwarded_port, guest: 3000, host: 3000`
    4. `vagrant up`
- після успішного встановлення (`vagrant up`), відкрийте в браузері [http://localhost:3000/](http://localhost:3000/)
- також доступна адміністративна панель : [http://localhost:3000/admin](http://localhost:3000/admin) : 
    - Email: `admin@example.com`
    - Password : `password`
- якщо виникли проблеми із відображенням додатку, необхідно запустити сервер Rails : 
    1. `vagrant ssh`
    2. `cd /var/www/html/uwc2014`
    3. `rails s`


Source code
-----------

- На віртуальній машині `Vagrant` : 
    1. `vagrant ssh`
    2. `/var/www/html/uwc2014`
- `BitBucket` [https://bitbucket.org/delfit/uwc2014](https://bitbucket.org/delfit/uwc2014)


- - -

*Команда Delfit*