class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :client_name
      t.string :client_phone

      t.timestamps
    end
  end
end
