class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title, null: false
      t.string :image_url
      t.decimal :price, precision: 10, scale: 2, null: false
      t.text :description, null: false

      t.timestamps
    end
    # add_index :products, :title, unique: true
  end
end
