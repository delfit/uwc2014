class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :kind, null: false
      t.string :group, null: false
      t.references :product, index: true, null: false

      t.timestamps
    end

    add_index :events, [:kind, :group]
  end
end
