class AddProductCounters < ActiveRecord::Migration
  def change
    add_column :products, :bought_count, :integer, default: 0, null: false
    add_column :products, :viewed_count, :integer, default: 0, null: false
    add_column :products, :carted_count, :integer, default: 0, null: false
  end
end